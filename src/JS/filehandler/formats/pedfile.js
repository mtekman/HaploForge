
var Pedfile = {

    __tmpfamdata : {}, // fid --> stored position

    __header_text: "FamID\tPerID\tFathID\tMothID\tGend\tAff.\tPos\tName\tNotes",

    import: function(text_unformatted){

        var text = text_unformatted.split('\n');
        var header = text[0].trim() // check all headers are there

        if (header != Pedfile.__header_text){
            utility.notify("Error", "Header line should be:" + Pedfile.__header_text, 10)
            console.log("Error:", Pedfile.__header_text)
            return(-1);
        }

        for (var l=1; l< text.length; l++)
        {
            var line = text[l];
            if (line.length < 5 ){continue};

            // Split in to Data and Metadata parts
            var data_and_meta = line.split('\t'); //don't trim to keep empty sections

            // family lines first
            if (data_and_meta[8] == "Root coordinates"){ // Messy!
                let fid = data_and_meta[0].trim(),
                    pos = data_and_meta[6].split(",").map(x => Number(x));
                Pedfile.__tmpfamdata[fid] = {x: pos[0], y: pos[1] };  // passed onto init_graph
                continue;
            }

            // Selection rectangles
            if (data_and_meta[8] == "Selection Groups"){ // Messy!
                let fid = data_and_meta[0].trim(),
                    title = data_and_meta[6].trim(),
                    col_pids = data_and_meta[7].split(':'),
                    colour = col_pids[0],
                    pids = col_pids[1].split(",").map(x => parseInt(x))

                if (Pedfile.__tmpfamdata[fid].groups === undefined){
                    Pedfile.__tmpfamdata[fid].groups = []
                }
                Pedfile.__tmpfamdata[fid].groups.push({title: title,
                                                       pids: pids,
                                                       color: colour})
                continue;
            }


            //Handle Person info
            var people_info = data_and_meta.slice(0,6).map(x => Number(x));
            var data_part = data_and_meta.slice(6,9);

            var fam = people_info[0], id  = people_info[1],
                pat = people_info[2], mat = people_info[3],
                sex = people_info[4], aff = people_info[5];

            var pers = new Person(id, sex, aff, mat, pat);
            familyMapOps.insertPerc(pers, fam);

            var meta = {}
            let pos = data_part[0].split(',').map(x => Number(x));
            meta.x = pos[0]
            meta.y = pos[1]
            meta.name = data_part[1]
            meta.notes = data_part[2].replace('||','\n'); // newlines create issues

            // Holds graphics, person's name, other meta
            familyMapOps.getPerc(id,fam).stored_meta = meta;
        }
    },

    sanity_check: function(){
        let unrelated = {}; //
        let checked = {}; // key of checked pairs

        familyMapOps.foreachfam(function(fid,fam_group){
            familyMapOps.foreachperc(function(pid1, fid, perc1){
                familyMapOps.foreachperc(function(pid2, fid, perc2){
                    if (pid1 === pid2){ return };

                    if (!(fid in checked)){
                        checked[fid] = {};
                    }

                    if (!(pid1 in checked)){
                        checked[fid][pid1] = {}
                    }

                    if (!(pid2 in checked)){
                        checked[fid][pid2] = {}
                    }

                    if (checked[fid][pid1][pid2] || checked[fid][pid2][pid1]){ return  };

                    checked[fid][pid1][pid2] = true;
                    checked[fid][pid2][pid1] = true;

                    let res = familyMapOps.areConnected(fid, pid1, pid2);
                    if (!res){
                        if (!(fid in unrelated)){
                            unrelated[fid] = {}
                        }
                        if (!(pid1 in unrelated[fid])){
                            unrelated[fid][pid1] = {}
                        }
                        if (!(pid2 in unrelated[fid])){
                            unrelated[fid][pid2] = {}
                        }
                        unrelated[fid][pid1][pid2] = true;
                        unrelated[fid][pid2][pid1] = true;
                    }
                }, fid);
            }, fid);
        });

        // Go through unrelated connections and single out the main targets.
        let mentions = {};

        for (var fid in unrelated){
            mentions[fid] = {};
            for (var id1 in unrelated[fid]){

                let targets = unrelated[fid][id1];
                for (var targ in targets){
                    if (!(targ in mentions[fid])){
                        mentions[fid][targ] = 0
                    }
                    mentions[fid][targ] += 1
                }
            }
        }

        return mentions;
    },

    exportToTab: function(store_graphics){
        exportToTab(Pedfile.export(store_graphics));
    },


    export: function(event)  // event is ignored
    {
        let allconnected = Pedfile.sanity_check();
        console.log(allconnected, Object.keys(allconnected), Object.keys(allconnected) > 0);

        if (Object.keys(allconnected).length > 0){
            utility.yesnoprompt(
                "Unconnected Individuals Detected",
                "These will be truncated from the pedigree. Continue with save?",
                "Yes", function(){},
                "No", function(){
                    return -1;
                }
            );
        }

        var text = Pedfile.__header_text + "\n";

        // Family-header specific
        var fid_array = []
        uniqueGraphOps.foreachfam(function(fid,fam_group){
            let posinfo = fam_group.group.getAbsolutePosition(),
                strinfo = fid + "\t\t\t\t\t\t" +
                Math.floor(posinfo["x"]) + "," + Math.floor(posinfo["y"]) +
                "\t\tRoot coordinates";
            fid_array.push(strinfo);

            // Print out selection groups too
            uniqueGraphOps.foreachselection(function(selection, key){
                let sels = selection._export(),
                    fam = sels.fam,
                    title = sels.title || "",
                    color = sels.color,
                    pids = sels.pids;

                var strout = fam + "\t\t\t\t\t\t" +
                    title + "\t" + color + ":" + pids.join(",") + "\tSelection Groups";
                fid_array.push(strout)
            }, fid)
        });
        text += fid_array.join('\n');

        // Person specific
        familyMapOps.foreachperc(function(pid, fid, perc){
            var array = [
                fid,
                perc.id,
                perc.father.id || 0,
                perc.mother.id || 0,
                perc.gender, perc.affected
            ]

            var gfx = uniqueGraphOps.getNode(pid, fid);

            if (gfx===-1 || gfx.graphics === null){
                console.log("[Error]", pid, fid,
                            "does not have any graphics...");
                array.push("","","")
            }
            else {
                var meta = gfx.graphics.getPosition();
                meta.name = perc.name
                meta.notes = perc.notes
                array.push(Math.floor(meta["x"]) + "," +
                           Math.floor(meta["y"]),
                           meta.name,
                           (meta.notes !== null) ? meta.notes.replace('\n','||') : "")
            }
            text += "\n"+ array.join('\t');
        });

        return text;
    },


    pedigreeChanged: function(){
        var current_pedigree = Pedfile.export(true), /* local saves _always_ store graphics */
            stored_pedigree = localStorage.getItem(localStor.ped_save);

        //      console.log("current", current_pedigree);
        //      console.log("stored", stored_pedigree);

        if (current_pedigree.trim().length < 1){
            return false;
        }

        if (stored_pedigree == null){
            return true;
        }

        if (current_pedigree !== stored_pedigree){
            return true;
        }
        return false;
    },
}