function determineBoundaries(x, w, y, h, margin = 5){
    var xbound = x + w,
        ybound = y + h;

    var left = ((xbound > x) ? x : xbound) - margin,
        right = ((xbound <= x) ? x : xbound) + margin,
        top = ((ybound > y) ? y : ybound) - margin,
        bottom = ((ybound <= y) ? y : ybound) + margin;

    return({left:left, right:right, top:top, bottom:bottom})
}

function addNodeGroup(fid, pids, rect=null, nodes=null, color=null, title=null){
    let key = fid + ":" +  pids.join(","),
        ng = new NodeGroup(fid, pids,
                           rect, nodes, key, color, title)
    if (uniqueGraphOps._map[fid].selections === undefined){
        uniqueGraphOps._map[fid].selections = {}
    }
    uniqueGraphOps._map[fid].selections[key] = ng
}


class NodeGroup {
    /* This is the coloured rectangle that surrounds a group of nodes */

    constructor(familyID, sel_pids, sel_rect=null, sel_nodes=null,
                key=null, color=null, title=null) {
        this._family = familyID

        this._group = null; /* Kinetic Group */
        this._rect = null; /* Kinetic outline */
        this._text = null; /* Kinetic title */
        this._modify = null; /* Kinetic corner close */

        this._selpids = sel_pids; /* Perc IDS */

        if (sel_rect === null){
            this._selnodes = []
            /* _selnodes are populated by below function */
            this._selrect = this._determineSelRectBounds(sel_pids)
        } else {
            this._selrect = sel_rect; /* Selection Rectangle */
            this._selnodes = sel_nodes; /* Selected Nodes */
        }

        this._key = key
        this._createGroup(color, title)
        // Enable nodes to update group on rect
        this._enableNodeListeners()
    }

    _enableNodeListeners(){
        var _this = this
        for (var n=0; n < this._selnodes.length; n++){
            let node = this._selnodes[n].graphics;
            node.on("dragend", function updater(event){
                _this.updateGroupBoundaries()
                main_layer.draw()
            })
        }
    }

    _disableNodeListeners(){
        for (var n=0; n < this._selnodes.length; n++){
            let node = this._selnodes[n].graphics;
            node.off("dragend", function updater(event){})
        }
    }

    _determineSelRectBounds(sel_pids){
        let fid = this._family,
            sel_rect = {x1:null,y1:null,x2:null,y2:null};

        for (var p=0; p < sel_pids.length; p++){
            let pid = sel_pids[p],
                node = uniqueGraphOps.getNode(pid, fid);

            let n_xy = node.graphics.gfx.getAbsolutePosition(),
                n_wt = node.graphics.gfx.getWidth(),
                n_ht = node.graphics.gfx.getHeight();

            let person = determineBoundaries(
                n_xy.x - n_wt, 2 * n_wt,
                n_xy.y - n_ht, 2 * n_ht
            )

            if ((sel_rect.x1 == null) || (person.left < sel_rect.x1)){sel_rect.x1 = person.left;}
            if ((sel_rect.y1 == null) || (person.top < sel_rect.y1)){sel_rect.y1 = person.top;}
            if ((sel_rect.x2 == null) || (person.right > sel_rect.x2)){sel_rect.x2 = person.right;}
            if ((sel_rect.y2 == null) || (person.bottom > sel_rect.y2)){sel_rect.y2 = person.bottom;}

            this._selnodes.push(node)
        }
        return(sel_rect)
    }

    updateGroupBoundaries(junk){
        var sel_rect = {x1:null,y1:null,x2:null,y2:null},
            abspos = main_layer.getAbsolutePosition(),
            absscale = main_layer.scale();


        for (var n=0; n < this._selnodes.length; n++){
            let node = this._selnodes[n];
            let n_xy = node.graphics.gfx.getAbsolutePosition(),
                n_wt = node.graphics.gfx.getWidth(),
                n_ht = node.graphics.gfx.getHeight();

            let person = determineBoundaries(
                n_xy.x - n_wt, 2 * n_wt,
                n_xy.y - n_ht, 2 * n_ht
            )

            if ((sel_rect.x1 == null) || (person.left < sel_rect.x1)){sel_rect.x1 = person.left;}
            if ((sel_rect.y1 == null) || (person.top < sel_rect.y1)){sel_rect.y1 = person.top;}
            if ((sel_rect.x2 == null) || (person.right > sel_rect.x2)){sel_rect.x2 = person.right;}
            if ((sel_rect.y2 == null) || (person.bottom > sel_rect.y2)){sel_rect.y2 = person.bottom;}
        }
        this._selrect = sel_rect;
        let swidth = sel_rect.x2 - sel_rect.x1,
            sheight = sel_rect.y2 - sel_rect.y1;

        this._group.setX(sel_rect.x1 - abspos.x)
        this._group.setY(sel_rect.y1 - abspos.y)
        this._group.setDraggable(true)

        this._rect.setX(0)
        this._rect.setY(0)
        this._rect.setWidth(swidth)
        this._rect.setHeight(sheight)

        this._modify.setX(swidth - 6)
        this._modify.setY(-6)
        this._modify.setWidth(12)
        this._modify.setHeight(12)
    }

    getGroup(){
        return({
            title : this._text.getText(),
            color : this._rect.getStroke()
        })
    }

    setGroup(grp, _this){
        _this._text.setText(grp.name)
        _this._text.setFill(grp.color)
        _this._modify.setFill(grp.color)
        _this._rect.setStroke(grp.color)
        main_layer.draw()
    }

    _createGroup(color, title){
        var _this = this,
            abspos = main_layer.getAbsolutePosition(),
            absscale = main_layer.scale(),
            sel_rect = _this._selrect,
            swidth = sel_rect.x2 - sel_rect.x1,
            sheight = sel_rect.y2 - sel_rect.y1;

        this._group = new Konva.Group({
            x: sel_rect.x1 - abspos.x,
		    y: sel_rect.y1 - abspos.y,
            draggable: true
        })
        this._rect = new Konva.Rect({
            x: 0, y:0,
            width: swidth, height: sheight,
		    stroke: color || "#00dd00",
            strokeWidth: 1,
        })
        this._text = new Konva.Text({
            x: 0, y: -13, // appear above line
            fill: color || 'black',
            text: title || "",
        })
        this._modify = new Konva.Rect({
            x: swidth - 6,
            y: - 6,
            width: 12, height: 12,
            fill: color || "#00dd00",
            stroke: "black"
        })

        this._group.add(this._rect)
        this._group.add(this._modify)
        this._group.add(this._text)

        main_layer.add(this._group)
        this._group.moveToBottom()
        main_layer.draw();

        this._sel_start = {x:null, y:null};
        // Store starting positions to calculate
        // different during dragmove.
        this._rect.on("mouseover", MouseStyle.changeToMove)
        this._rect.on("mouseout", MouseStyle.restoreCursor)

        this._group.on("mousedown", function(event){_this._storeStartingPoints(event)})
        this._group.on("dragmove", function(event){_this._dragRectangle(event)})
        this._group.on("dragend click", function(event){
            _this._updateLines(event)
            main_layer.draw()
        })

        this._modify.on("mouseover", function(){
            MouseStyle.changeToPointer()
            this._oldcolor = this.getFill()
            this.setFill("red")
            main_layer.draw()
        })
        this._modify.on("mouseout", function(){
            MouseStyle.restoreCursor()
            this.setFill(this._oldcolor || "green")
            main_layer.draw()
        })

        this._modify.on("click", function(event){
            _this._updateStuff(event, _this)
        })
    }

    _export(){
        let title_color = this.getGroup();
        return({pids: this._selpids,
                fam: this._family,
                title: title_color.title,
                color: title_color.color})
    }

    _deleteSelf(){
        this._text.destroy()
        this._rect.destroy()
        this._modify.destroy()
        this._group.destroy()
        this._disableNodeListeners()
        delete uniqueGraphOps._map[this._family].selections[this._key]
        main_layer.draw()
    }

    _updateStuff(event, _this){
        groupProps.display(_this.getGroup(),
                           function onsubmit(grp, that){
                               _this.setGroup(grp, _this)
                           },
                           function onclose(){
                               _this._deleteSelf()
                           })
        main_layer.draw()
    }

    _storeStartingPoints(event){
        this._sel_start.x = event.evt.clientX;
        this._sel_start.y = event.evt.clientY;
        this._group.startx = this._group.getX()
        this._group.starty = this._group.getY()
        for (var n=0; n < this._selnodes.length; n++){
            let nod = this._selnodes[n];
            nod.start_pos = nod.graphics.getPosition()
        }
    }

    _dragRectangle(event){
        let dx = Math.floor((event.evt.clientX - this._sel_start.x)/grid_rezX)*grid_rezX,
            dy = Math.floor((event.evt.clientY - this._sel_start.y)/grid_rezY)*grid_rezY;

        this._group.setX(this._group.startx + dx)
        this._group.setY(this._group.starty + dy)

        // move nodes but don't update lines
        for (var n = 0; n < this._selnodes.length; n++){
            let nod = this._selnodes[n],
                newx = nod.start_pos.x + dx,
                newy = nod.start_pos.y + dy;
            nod.graphics.setX(newx)
            nod.graphics.setY(newy)
        }
    }

    _updateLines(event) {
        let pids = this._selpids,
            fid = this._family;
        for (var p=0; p < pids.length; p++){
            redrawNodes(pids[p], fid, true);
        }
        main_layer.draw();
    }
}



class SelectNodes {

    constructor(familyID) {

	    this._family = familyID

	    this._layer = null; /*Layer*/
	    this._tmpRect = null; /* Rectangle to detect mousemove */

	    this._tmpSelectRect = null; /*Rectangle to select nodes*/
	    this._startPoint = {x:-1,y:-1};
	    this._endPoint = {x:-1,y:-1};
		this._onaddhit = null;
		this._ondelhit = null;
    }

	_delHitRect() {
		if (this._ondelhit !== null){
			this._ondelhit();
		}
		if (this._layer !== null){
			this._layer.destroy();
			this._layer = null;
		}
	}

    _addHitRect() {
        let spos = stage.getAbsolutePosition();

        this._layer = new Konva.Layer({
			width: stage.getWidth(),
			height:stage.getHeight(),
			x:-spos.x, y: -spos.y
		});
		this._tmpRect = new Konva.Rect({
			width: stage.getWidth(),
			height: stage.getHeight(),
			x:0, y:0, opacity: 0.1,
            fill: 'black'
		})

		stage.add( this._layer );
		this._layer.add(this._tmpRect);

        this.drawBackground();
        if (this._onaddhit !== null){
			this._onaddhit();
		}
		this._layer.draw();
        stage.draw()
    }

    init(){
		if (this._tmpSelectRect !== null){
			this.endSelectDraw();
		}
		this._addHitRect();
        MouseStyle.changeToArrowCursor()
	}

    drawBackground() {
        var _this = this
        _this.stage_draggable_state = stage.getDraggable();
		MouseResize.off()

        _this._tmpRect.on("mousedown", function(event, callback){
            _this.beginSelectDraw();
        })
    }

    _onmousemoveSelectRect (event){
		if (this._startPoint.x !== -1){
			var mouseX = Math.floor(event.evt.clientX),
			    mouseY = Math.floor(event.evt.clientY)

			this._endPoint = {
				x: mouseX, y: mouseY
			};
			this.updateSelectRect(mouseX, mouseY);
			this._layer.draw();
		}
    }

    beginSelectDraw() {
		MouseResize.off()

		var _this = this;

		_this._tmpSelectRect = new Konva.Rect({
			x: 0,
			y: 0,
			stroke: 'blue',
			strokeWidth: 3,
            dashArray : [10,5]
		});
        //_this._tmpSelectRect.setDashArrayEnabled(true);

		_this._layer.add(_this._tmpSelectRect);

		_this._tmpRect.on("mousedown", function(event){
			var mouseX = Math.floor(event.evt.clientX),
			    mouseY = Math.floor(event.evt.clientY);
            _this._startPoint.x = mouseX
            _this._startPoint.y = mouseY
            _this._tmpSelectRect.setX(mouseX)
            _this._tmpSelectRect.setY(mouseY)
        })

		_this._tmpRect.on("mousemove dragmove",
                          function(ev){_this._onmousemoveSelectRect(ev)})
		_this._tmpSelectRect.on("mousemove dragmove",
                                function(ev){_this._onmousemoveSelectRect(ev)})
		_this._tmpRect.on("mouseup dragend click",
                          function(ev){_this.endSelectDraw(ev)})
        _this._tmpSelectRect.on("mouseup dragend click",
                                function(ev){_this.endSelectDraw(ev)})
	}

    updateSelectRect(width, height) {
        this._tmpSelectRect.setWidth(width - this._startPoint.x);
        this._tmpSelectRect.setHeight(height - this._startPoint.y);
    }

    _getSelectedNodes (){
        let _this = this;
        let select = _this._getSelectionBounds(),
            sel_rect = {x1:null,y1:null,x2:null,y2:null};

        _this.sel_nodes = [];
        _this.sel_pid = [];

        uniqueGraphOps.foreachnode(function(pid, node){
            let n_xy = node.graphics.gfx.getAbsolutePosition(),
                n_wt = node.graphics.gfx.getWidth(),
                n_ht = node.graphics.gfx.getHeight();

            let person = determineBoundaries(
                n_xy.x - n_wt, 2 * n_wt,
                n_xy.y - n_ht, 2 * n_ht
            )

            if ((select.left < person.left) && (select.right > person.right) &&
                (select.top < person.top) && (select.bottom > person.bottom)){

                if ((sel_rect.x1 == null) || (person.left < sel_rect.x1)){sel_rect.x1 = person.left;}
                if ((sel_rect.y1 == null) || (person.top < sel_rect.y1)){sel_rect.y1 = person.top;}
                if ((sel_rect.x2 == null) || (person.right > sel_rect.x2)){sel_rect.x2 = person.right;}
                if ((sel_rect.y2 == null) || (person.bottom > sel_rect.y2)){sel_rect.y2 = person.bottom;}
                _this.sel_pid.push(pid);
                _this.sel_nodes.push(node);
            }
        }, _this._family)

        if (_this.sel_pid.length > 0){
            addNodeGroup(_this._family, _this.sel_pid, sel_rect, _this.sel_nodes)
        }
    }

    _getSelectionBounds(){
        var xy = this._tmpSelectRect.getAbsolutePosition(),
            w = this._tmpSelectRect.getWidth(),
            h = this._tmpSelectRect.getHeight();

        var bounds = determineBoundaries(xy.x, w, xy.y, h)
        return(bounds)
    }

	endSelectDraw(event) {
        if (this._tmpSelectRect == null){
            return 0
        }
        this._getSelectedNodes();
        this._delHitRect();

		if (this._tmpSelectRect !== null){
			this._tmpSelectRect.destroy();
			this._tmpSelectRect = null;
		}

		MouseStyle.restoreCursor();

		this._startPoint = {x:-1, y:-1}

		// restore draggable if previously set
		MouseResize.on()
		stage.setDraggable( this.stage_draggable_state );
	}
}